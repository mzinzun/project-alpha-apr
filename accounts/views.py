from django.shortcuts import render, redirect
from accounts.forms import LogInForm, SignUpForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Create your views here.


def user_login(request):
    if request.method == "POST":
        form = LogInForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data["password"]
            username = form.cleaned_data["username"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        form = LogInForm()
    if request.user.is_anonymous:
        no_user = True
    else:
        no_user = False
    context = {
        "form": form,
        "no_user": no_user,
    }
    return render(request, "userLogin.html", context)


def user_logout(request):
    print("user before", request.user)
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(username, password=password)
                login(request, user)
                return redirect("list_projects")
            else:
                raise ValueError("the passwords do not match")
    else:
        form = SignUpForm()
    if request.user.is_anonymous:
        no_user = True
    else:
        no_user = False
    context = {"form": form, "no_user": no_user}
    return render(request, "signup.html", context)
